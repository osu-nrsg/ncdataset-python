# Changelog

All notable changes to the project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2021-02-10

- Major release
- `get_scalar_var` works with string vars.

## [0.3.0] - 2020-11-13

### Added

- Issue #2: Track context manager depth so the dataset isn't closed until exiting the outermost `with` block.

## [0.2.3] - 2020-11-02

### Added

- haller pypi upload script
- _version.py

### Modified

- `__repr__` for `NCDataset` corrected to have `nc_kwargs`.

## [0.2.2_2] - 2020-10-02

### Added

- CHANGELOG.md
- README.md
- Get version from package version indirectly so `pyproject.toml` is the only home of the version

## 0.2.0

### Changed

- **Breaking Change** - Changed keyword argument `createonly=False` to `keepopen=True`.

## 0.1.0

- Initial development

[Unreleased]: https://gitlab.com/osu-nrsg/ncdataset-python/-/compare/v1.0.0...master
[1.0.0]: https://gitlab.com/osu-nrsg/ncdataset-python/-/compare/v0.3.0...v1.0.0
[0.3.0]: https://gitlab.com/osu-nrsg/ncdataset-python/-/compare/v0.2.3...v0.3.0
[0.2.3]: https://gitlab.com/osu-nrsg/ncdataset-python/-/compare/v0.2.2_2...v0.2.3
[0.2.2_2]: https://gitlab.com/osu-nrsg/ncdataset-python/-/tags/v0.2.2_2
