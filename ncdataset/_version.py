import warnings


def get_version():
    try:
        import importlib.metadata as importlib_metadata
    except ImportError:
        try:
            import importlib_metadata
        except ImportError:
            warnings.warn("importlib-metadata package needed to get the version of this package.")
            return "unknown"
    try:
        return importlib_metadata.version(__name__)
    except importlib_metadata.PackageNotFoundError:
        pass  # OK, the package isn't installed

    # Try reading the pyproject.toml file
    from pathlib import Path

    pp_toml: Path = Path(__file__).absolute().parent.parent / "pyproject.toml"
    if pp_toml.is_file():
        try:
            import toml
        except ImportError:
            pass
        else:
            # we have toml
            pyproject = toml.load(pp_toml)
            try:
                return pyproject["tool"]["poetry"]["version"]
            except KeyError:
                pass
    return "unknown"  # got here if we couldn't get the version through the package metadata or TOML file.
