import warnings

from ._version import get_version
from .ncdataset import NCDataset, NCDatasetError

__version__ = get_version()
