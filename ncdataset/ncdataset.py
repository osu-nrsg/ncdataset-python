"""This module contains the NCDataset class to make working with NetCDF Datasets a little nicer.
"""
from pathlib import Path
from typing import Any, Dict, Union

import netCDF4


class NCDatasetError(Exception):
    pass


# module-level variables
ds_closed_err = NCDatasetError("The dataset is not open and thus its contents are not accessible.")
auto_mask: bool = False


class NCDataset(netCDF4.Dataset):
    """Wrapper for NetCDF4 Dataset that keeps track of the filename and keyword arguments, even when it
    closes.

    Examples
    --------

    >>> ds = NCDataset("example.nc", mode="w", keepopen=False, diskless=True, persist=True)
    >>> print(ds)
    NCDataset[netCDF4.Dataset]
    nc_path: example.nc
    nc_kwargs: {'diskless': True, 'persist': True, 'mode': 'w', 'clobber': False}
    (closed)
    >>> # Since mode was "w", mode is now automatically "a" (unless we set clobber=False)
    >>> # Changes to the kwargs can be made with update_params or just __call__
    >>> with ds(diskless=False):
    ...     # Can augment the netCDF4.Dataset here, just like netCDF4.Dataset
    ...     ds.createDimension("time", 1024)
    ...     print(ds)
    ...
    NCDataset[netCDF4.Dataset]
    nc_path: example.nc
    nc_kwargs: {'diskless': False, 'persist': True, 'mode': 'a', 'clobber': False}
    <class 'hpx_radar_recorder.ncdataset.NCDataset'>
    root group (NETCDF4 data model, file format HDF5):
        dimensions(sizes): time(1024)
        variables(dimensions):
        groups:
    """

    _private_atts = [
        "nc_path",
        "nc_kwargs",
        "auto_mask",
        "_ctxmgr_depth",
    ]  # for __getattr__ and __setattr__
    nc_path: Path
    nc_kwargs: Dict[str, Any]
    auto_mask: bool
    _ctxmgr_depth: int

    def __init__(self, nc_path: Union[Path, str], mode: str = "r", keepopen: bool = True, **kwargs):
        """Initialize the NCDataset object.

        Opens or creates a netCDF4.Dataset and optionally leaves it open for modification.
        The file path and keyword arguments are retained for future use of the file.

        Parameters
        ----------
        nc_path : Path or str
            Path to open or create the netCDF dataset.
        mode : str, optional
            See `netCDF4.Dataset.__init__()`. For compatibility with `Dataset(path_to_ds, "r")` syntax.
        keepopen : bool, optional
            If True, the dataset is left open after creating/opening. (default)
            If False, the dataset file is created/opened (depending on mode) and closed again. This confirms
            that it exists and saves the keyword arguments for future interaction.
        **kwargs
            Other arguments to pass to `netCDF4.Dataset.__init__()`. See the netCDF4-python docs for details.
        """
        self.nc_path = Path(nc_path)
        self.auto_mask = auto_mask  # import module-level default
        kwargs["mode"] = mode
        self.nc_kwargs = kwargs
        self.open()
        if not keepopen:
            self.close()
        self._ctxmgr_depth = 0

    def get_scalar_var(self, varname: str, **kwargs):
        """Get a variable, if it exists. If not and 'default' is provided, return default. otherwise allow
        the exception from netCDF4 to be raised (IndexError). Also netCDF4 will raise an IndexError if
        the variable is not scalar.
        """
        if "default" in kwargs:
            try:
                var_ = self[varname]
            except IndexError:
                return kwargs["default"]
        else:
            var_ = self[varname]
        try:
            return var_.getValue().item()
        except AttributeError:  # "scalar" strings don't have item()
            return var_.getValue()

    # Have to override __getattr__ and __setattr__ to allow access to this class's attributes.
    def __getattr__(self, name):
        if name in self._private_atts:
            return self.__dict__[name]
        else:
            if self.isopen():
                return super().__getattr__(name)
            else:
                raise ds_closed_err

    def __setattr__(self, name, value):
        if name in self._private_atts:
            self.__dict__[name] = value
        else:
            if self.isopen():
                super().__setattr__(name, value)
            else:
                raise ds_closed_err

    def __getitem__(self, item):
        if self.isopen():
            return super().__getitem__(item)
        else:
            raise ds_closed_err

    def __contains__(self, varname):
        raise NCDatasetError(
            "`'thing' in ds` is not valid for NCDataset. Consider `'varname' in ds.variables`"
            " or `'attrname' in ds.ncattrs()`."
        )

    def __enter__(self):
        self._ctxmgr_depth += 1
        self.open()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._ctxmgr_depth -= 1
        if self._ctxmgr_depth <= 0:
            self.close()

    def __call__(self, **kwargs):
        """If the dataset is closed, __call__ can be used to change the calling kwargs.

        Parameters
        ----------
        **kwargs
            Arguments to pass to `update_params()`.

        Returns
        -------
        NCDataset
            This NCDataset instance.

        """
        self.update_params(**kwargs)
        return self

    def __repr__(self):
        if self.isopen():
            ds_repr = super().__repr__()
        else:
            ds_repr = "(closed)"
        return (
            f"{self.__class__}[netCDF4.Dataset]"
            f"\nnc_path: {self.nc_path}"
            f"\nnc_kwargs: {self.nc_kwargs}"
            f"\n{ds_repr}"
        )

    def update_params(self, replace_kwargs=False, **kwargs):
        """Update keyword arguments passed to `netCDF4.Dataset.__init__()

        Parameters
        ----------
        replace_kwargs : bool, optional
            If True, the (k,v) pairs in `**kwargs` replace the previous keyword arguments. Otherwise,
            `**kwargs` amends the existing arguments. Default is False.
        **kwargs
            Keyword arguments for `netCDF4.Dataset.__init__()`.

        Raises
        ------
        NCDatasetError
            If called when the dataset is open.
        """
        if self.isopen():
            raise NCDatasetError("Cannot update calling parameters when dataset is open.")
        if replace_kwargs:
            self.nc_kwargs = kwargs
        else:
            self.nc_kwargs.update(kwargs)

    def open(self, **kwargs):
        """Reopen the dataset with the same or new arguments.

        By default `clobber=False` (see doc for `netCDF4.Dataset.__init__()`).
        If `clobber==True`, it will revert to default (`False`) after the dataset is closed. Other kwargs
        will update the default kwargs.

        Parameters
        ----------
        **kwargs
            Updates to kwargs to pass to `netCDF4.Dataset.__init__()`.
        """
        if kwargs:
            self.update_params(**kwargs)
        if not self.isopen():
            super().__init__(self.nc_path, **self.nc_kwargs)
            self.set_auto_mask(self.auto_mask)

    def close(self):
        """Close the dataset.
        Calls netCDF4.Dataset.close() but only if the dataset it open.
        Resets clobber to False and mode to "a" if it was "w".
        """
        if self.isopen():
            super().close()
        self.nc_kwargs["clobber"] = False
        if self.nc_kwargs.get("mode", "r") == "w":
            self.nc_kwargs["mode"] = "a"

    def set_auto_mask(self, true_or_false: bool):
        """
        See netCDF4.Dataset.set_auto_mask().
        Value preserved across dataset close/open, and OK to set with closed dataset.
        """
        if self.isopen():
            super().set_auto_mask(true_or_false)
        self.auto_mask = true_or_false
