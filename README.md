# NCDataset - An extension of netCDF4.Dataset for Python

This package contains a single class--NCDataset, that is a subclass of netCDF4.Dataset from the
[netCDF4 python library](http://unidata.github.io/netcdf4-python/netCDF4/index.html#netCDF4.Dataset)
(hereafter `nc4.Dataset`). The primary goal of NCDataset has been to make dataset objects reusable--that is
to be able to re-open a closed dataset without re-specifying all the opening arguments. I'd be happy to see
Unidata roll these into netCDF4.Dataset!

## Features of NCDataset

* Track dataset path and initialization args (e.g. `mode`, `diskless`, `clobber` etc.) for re-use of the
dataset object in multiple `with` blocks.
* If the dataset is opened in mulitple `with` blocks, do not close it until leaving the outermost `with`
  block.
* Each dataset gets an `auto_mask` attribute that sets the `set_auto_mask` value on the dataset when the
dataset is opened. By default, this is set to `False` (in contrast with `nc4.Dataset`), but may be changed
by the module-level `auto_mask` variable.
* `keepopen=False` arg allows the dataset object to be created/opened but then closed immediately (for
later use in a `with` block)
* `update_args(**kwargs)` can be used to update calling keyword args (though not the path to the dataset.)
  * `__call__(**kwargs)` calls `update_args(**kwargs)`.
* `open(**kwargs)` opens the dataset (if closed), updating the calling arguments with `**kwargs`. If the
dataset is already open it does nothing.
* To help prevent accidental dataset overwrite:
  * After initialization, if `mode` is set to `"w"` it is automatically switched to `"a"` unless `clobber`
    is set to `True`.
  * If `clobber` is set to `True` at any point it reverts to `False` after the object is closed.
* `__repr__` includes the dataset path and the open/closed state in addition to the stuff from `nc4.Dataset`
* `__getitem__` and `__getattr__` first check if the dataset is open and raise an `NCDatasetError` if the
dataset is closed.
* `__contains__` raises an `NCDatasetError` if the caller tries to test something is `in` a dataset,
suggesting the use of `"varname" in ds.variables` or `attrname in ds.ncattrs`.

## Examples

```python
>>> from ncdataset import NCDataset
>>> ds = NCDataset("example.nc", mode="w", keepopen=False, diskless=True, persist=True)
>>> print(ds)
NCDataset[netCDF4.Dataset]
kwargs: {'diskless': True, 'persist': True, 'mode': 'w', 'clobber': False}
(closed)
>>> # Since mode was "w", mode is now automatically "a" (unless we set clobber=False)
>>> # Changes to the kwargs can be made with update_params or just __call__
>>> with ds(diskless=False):
...     # Can augment the netCDF4.Dataset here, just like netCDF4.Dataset
...     ds.createDimension("time", 1024)
...     print(ds)
...
NCDataset[netCDF4.Dataset]
kwargs: {'diskless': False, 'persist': True, 'mode': 'a', 'clobber': False}
<class 'hpx_radar_recorder.ncdataset.NCDataset'>
root group (NETCDF4 data model, file format HDF5):
    dimensions(sizes): time(1024)
    variables(dimensions):
    groups:
>>> # The dataset remains open in nested context managers
>>> print(f"DATASET NOT YET OPENED, ds.isopen(): {ds.isopen()}")
>>> with ds:
...     print(f"OUTER with BLOCK, ds.isopen(): {ds.isopen()}")
...     with ds:
...         print(f"INNER with BLOCK, ds.isopen(): {ds.isopen()}")
...     print(f"EXITED INNER with BLOCK, ds.isopen(): {ds.isopen()}")
... print(f"EXITED OUTER with BLOCK, ds.isopen(): {ds.isopen()}")
DATASET NOT YET OPENED, ds.isopen(): False
OUTER with BLOCK, ds.isopen(): True
INNER with BLOCK, ds.isopen(): True
EXITED INNER with BLOCK, ds.isopen(): True
EXITED OUTER with BLOCK, ds.isopen(): False
```
